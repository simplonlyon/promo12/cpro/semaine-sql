
-- Faire une procédure qui affiche le ratio Homme/Femme dans l'entreprise
USE `employees`;
DROP procedure IF EXISTS `mf_ratio`;

DELIMITER $$
USE `employees`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `mf_ratio`()
BEGIN
	
    DECLARE total_employees INT;
    DECLARE total_f INT;
    
    SELECT COUNT(*) INTO total_employees FROM employees;
    SELECT COUNT(*) INTO total_f FROM employees WHERE gender='F';
    
    SELECT total_f * 100 / total_employees AS ratio_mf;
	
    
END$$

DELIMITER ;


USE `employees`;
DROP procedure IF EXISTS `avg_raise`;

-- Faire une procédure qui calcule l'augmentation de salaire moyenne entre deux années données

DELIMITER $$
USE `employees`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `avg_raise`()
BEGIN

	SELECT AVG(raise) FROM (SELECT MAX(salary) - MIN(salary) AS raise, COUNT(*) as nb  FROM salaries 
	WHERE from_date BETWEEN '1990-01-01' AND '1991-12-31' GROUP BY emp_no HAVING nb =2) AS raises;
    

END$$

DELIMITER ;

