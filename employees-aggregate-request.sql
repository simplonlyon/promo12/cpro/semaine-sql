USE employees;

-- 1. Faire une requête qui va compter le nombre de femme et d'homme parmi les employés
SELECT COUNT(*) as count, gender FROM employees GROUP BY gender;

-- 2. Même genre, mais un peu moins utile, afficher combien d'employees ont le même nom de famille
SELECT COUNT(*) as count, last_name FROM employees GROUP BY last_name;

-- 3. Afficher les employees et le nombre de title qu'ielles ont eu dans leur carrière dans l'entreprise
SELECT e.*, COUNT(*) as titles_count FROM employees e INNER JOIN titles t ON e.emp_no=t.emp_no GROUP BY e.emp_no;

-- 4. Afficher le salaire moyen actuel de l'entreprise
SELECT AVG(salary) as average_salary FROM salaries WHERE to_date >= now();

-- 5. Afficher le salaire total versé à chaque employees depuis le début de leur carrière
SELECT e.*, SUM(salary) as total_salary FROM salaries s RIGHT JOIN employees e ON s.emp_no=e.emp_no GROUP BY s.emp_no;

-- 6. Afficher le salarié qui a le salaire le plus élevé de l'entreprise et son title actuel
SELECT e.*, t.title, MAX(s.salary) as highest_salary FROM employees e INNER JOIN salaries s ON s.emp_no = e.emp_no 
INNER JOIN titles t ON t.emp_no = e.emp_no WHERE t.to_date >= now();

-- 7. Afficher le nombre de manager qu'ont eux chaque departments
SELECT d.*, COUNT(*) as manager_count FROM departments d INNER JOIN dept_manager dm ON d.dept_no=dm.dept_no GROUP BY dept_no;

-- 8. Même genre, afficher le nombre d'employees actuels par departments
SELECT d.*, COUNT(*) as employees_count FROM departments d INNER JOIN dept_emp de ON d.dept_no=de.dept_no WHERE de.to_date >= now() GROUP BY dept_no;

-- 10. Afficher les employees et tous leur titles concaténés dans une colonne
SELECT e.*, group_concat(t.title) AS titles FROM employees e INNER JOIN titles t ON t.emp_no=e.emp_no GROUP BY emp_no;

-- 11. Afficher les employees dont le salaire moyen est supérieur à 80000 (elle prend du temps à s'exécuter cette requête)
SELECT e.*, AVG(salary) AS average_salary FROM employees e INNER JOIN salaries s ON s.emp_no=e.emp_no GROUP BY emp_no HAVING average_salary > 80000;

-- 9. Afficher les manager les mieux payé de chaque departments
SELECT d.*, concat(e.first_name,' ', e.last_name) AS manager, MAX(s.salary) as highest_salary FROM employees e INNER JOIN dept_manager dm ON dm.emp_no=e.emp_no 
INNER JOIN departments d ON dm.dept_no=d.dept_no INNER JOIN salaries s ON s.emp_no = e.emp_no GROUP BY d.dept_no;
