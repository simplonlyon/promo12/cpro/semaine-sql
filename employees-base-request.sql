USE employees;
-- 1. Afficher tous les employees dont le prénom commence par Su
SELECT * FROM employees WHERE first_name LIKE "Su%";

-- 2. Afficher les employees embauché·e·s en 2000 et après
SELECT * FROM employees WHERE hire_date >= "2000-01-01";

-- 3. Afficher les employees et leurs titles actuel
SELECT e.emp_no, t.title, e.first_name, e.last_name FROM employees e 
INNER JOIN titles t ON e.emp_no=t.emp_no WHERE t.to_date >= now();

-- 5. Afficher le nom des Departments et le nom de leur manager actuel·le
SELECT e.first_name, e.last_name, d.dept_name FROM employees e INNER JOIN dept_manager dm ON dm.emp_no=e.emp_no INNER JOIN departments d 
ON d.dept_no = dm.dept_no WHERE dm.to_date >= now();


-- 6. Ajouter vous comme employees dans le department Development avec comme title CDA (attention, les id ne sont pas en auto_increment)
INSERT INTO employees (emp_no, first_name, last_name, birth_date, hire_date, gender) VALUES (500001, 'Jean', 'Demel', '1990-10-10', curdate(), 'M');

INSERT INTO titles (emp_no, title, from_date, to_date) VALUES (500001, 'CDA', curdate(), '9999-01-01');
INSERT INTO dept_emp (emp_no, dept_no, from_date, to_date) VALUES (500001, 'd005', curdate(), '9999-01-01');